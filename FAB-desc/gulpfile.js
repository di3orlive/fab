var gulp = require('gulp'),
    plumber = require('gulp-plumber'),
    notify = require("gulp-notify"),
    util = require('gulp-util'),
    sass = require('gulp-sass'),
    imageop = require('gulp-image-optimization'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    minifycss = require('gulp-minify-css'),
    fileinclude = require('gulp-file-include'),
    server = require('gulp-server-livereload'),
    autoprefixer = require('gulp-autoprefixer'),
    gutil = require('gulp-util'),
    uglify = require('gulp-uglify'),
    fs = require('fs'),
    wiredep = require('wiredep').stream,
    log = util.log;

gulp.task('bower', function () {
    gulp.src('./src/**/*.html')
        .pipe(wiredep({directory: './src/bower'}))
        .pipe(gulp.dest('./src/'));


    gulp.src('./src/bower/**/*.*')
        .pipe(gulp.dest('./www/bower'));
});

gulp.task('sass', function () {
    var sassErr = sass({});
    sassErr.on('error',function(e){
        gutil.log(e);
        sassErr.end();
    });

    log('============Generate CSS files============');
    gulp.src('./src/sass/*.sass')
        .pipe(plumber({
            errorHandler: reportError
        }))
        .pipe(sass({style: 'expanded'}))
        .pipe(concat('main.css'))
        .pipe(autoprefixer('last 3 version', 'safari 5', 'ie 8', 'ie 9'))
        .pipe(gulp.dest('www/css'))
        .pipe(rename({suffix: '.min'}))
        .pipe(minifycss())
        .pipe(gulp.dest('./www/css/min'))
        .on('error', reportError);
});

gulp.task('js', function() {
    return gulp.src('./src/js/*.js')
        .pipe(concat('main.js'))
        .pipe(gulp.dest('www/js/'))
        .pipe(uglify())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('./www/js/min/'));
});

gulp.task('img', function(cb) {
    gulp.src('src/i/**/*.*')
        .pipe(imageop({
            optimizationLevel: 5,
            progressive: true,
            interlaced: true
        })).pipe(gulp.dest('./www/i')).on('end', cb).on('error', cb);
});

gulp.task('webserver', function () {
    gulp.src('./www/')
        .pipe(server({
            livereload: true,
            port: 11111,
            open: './www/index.html',
            directoryListing: {
                enable: true,
                path: 'www'
            }
        })
    )
});

gulp.task('release', function () {
    var number = gutil.env.number;
    //gulp release --number 0.1
    if (fs.existsSync('./releae/' + number)){
        return console.error('Number ' + number + ' already exists')
    }
    console.log('Making release ' + number + ' ');
    gulp.src('./www/**/*.*')
        .pipe(gulp.dest("./releases/" + number + '/'));
});

gulp.task('fileinclude', function() {
    gulp.src(['./src/**/*.html'])
        .pipe(fileinclude({
            prefix: '@@',
            basepath: '@file'
        }))
        .pipe(gulp.dest('./www/'));
});

gulp.task('watch', function () {
    gulp.watch('./src/**/*.html', ['fileinclude']);
    gulp.watch('./src/sass/**/*.sass', ['sass']);
    gulp.watch('./src/js/**/*.js', ['js']);
    gulp.watch('./src/i/**/*.*', ['img']);
    gulp.watch('bower.json', ['bower']);
});

gulp.task('default', function () {
    gulp.run('sass', 'js', 'webserver',  'watch');
});


//======================================================================================================================


var reportError = function (error) {
    notify({
        title: 'Error',
        message: 'Check the console.'
    }).write(error);

    console.log(error.toString());

    this.emit('end');
};