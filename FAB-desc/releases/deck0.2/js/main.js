'use strict';

(function() {
    var app = angular.module('fabDApp', ['oi.select']);

    app.controller('tabCtrl', ['$scope', function ($scope) {
        $scope.tab = 'tab1';

        $scope.setTab = function(newTab){
            $scope.tab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.tab === tabNum;
        };
    }]);


    app.controller('saloonsCtrl', ['$scope', function ($scope) {
        $scope.tab = 'tab1';

        $scope.setTab = function(newTab){
            $scope.tab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.tab === tabNum;
        };
    }]);


    app.controller('selectCtrl', ['$scope', function ($scope) {
        $scope.cityOgj = [
            {"id":1,"name":"City1"},
            {"id":2,"name":"City2"},
            {"id":3,"name":"City3"},
            {"id":4,"name":"City4"},
            {"id":5,"name":"City5"}
        ];
    }]);
//======================================================================================================================
//======================================================================================================================
//======================================================================================================================


})();