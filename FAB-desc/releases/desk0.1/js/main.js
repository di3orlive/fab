'use strict';

(function() {
    var app = angular.module('fabDApp', []);

    app.controller('tabCtrl', ['$scope', function ($scope) {
        $scope.tab = 'tab1';

        $scope.setTab = function(newTab){
            $scope.tab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.tab === tabNum;
        };
    }]);


    app.controller('saloonsCtrl', ['$scope', function ($scope) {
        $scope.tab = 'tab1';

        $scope.setTab = function(newTab){
            $scope.tab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.tab === tabNum;
        };
    }]);


//======================================================================================================================
//======================================================================================================================
//======================================================================================================================


})();