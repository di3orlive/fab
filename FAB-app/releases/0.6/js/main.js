'use strict';

(function() {
    var app = angular.module('fabApp', ['perfect_scrollbar']);

    app.controller('tabCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.searchTab = 'services';

        $scope.setTab = function(newTab){
            $scope.searchTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.searchTab === tabNum;
        };



        $http.get('services.json').success(function(response) {
            //debugger;
            $scope.servicesData = response.services;
        });
    }]);

//======================================================================================================================
//======================================================================================================================
//======================================================================================================================

    app.directive("fabAccordion", function ($timeout) {
        return {
            restrict: "A",
            link: function () {
                var onDomChange = function(){
                    $('.acc-title').on("click", function (e) {
                        var thisItemContent = $(this).next();


                        $('.acc-title.open').not($(this)).removeClass('open');
                        $('.acc-content.open').not(thisItemContent).removeClass('open').removeAttr('style');
                        thisItemContent.toggleClass('open');
                        $(this).toggleClass('open');


                        if (thisItemContent.hasClass('open')) {
                            thisItemContent.css({height: thisItemContent.height()});
                        } else  {
                            thisItemContent.removeAttr('style');
                        }
                    });
                };
                $timeout(onDomChange, 0);
            }
        };
    });

    app.directive("clickScroll", function () {
        return {
            restrict: "A",
            link: function (scope, element) {
                var scrollContent = angular.element(element).children('[data-scrollContent]'),
                    prevBtn = angular.element(element).children('[data-prevBtn]'),
                    nextBtn = angular.element(element).children('[data-nextBtn]'),
                    left = 0,
                    totalWidth = 0;

                scrollContent.children().each(function() {
                    totalWidth += $(this).width();
                });
                totalWidth -= 100;



                prevBtn.on('click', function(){
                    left += 100;
                    scrollContent.css({
                        transform: 'translateX(' + left + 'px)'
                    }).attr('data-left', left);


                    if(scrollContent.attr('data-left') >= 0){
                        scrollContent.css({
                            transform: 'translateX(0px)'
                        });
                        left = 0;
                    }
                });


                nextBtn.on('click', function(){
                    left -= 100;
                    scrollContent.css({
                        transform: 'translateX(' + left + 'px)'
                    }).attr('data-left', left);


                    if(scrollContent.attr('data-left') < -totalWidth){
                        scrollContent.css({
                            transform: 'translateX('+ -totalWidth +'px)'
                        });
                        left = -totalWidth;
                    }
                });
            }
        };
    });

})();