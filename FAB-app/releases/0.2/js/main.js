'use strict';

(function() {
    var app = angular.module('fabApp', []);

    app.controller('tabCtrl', ['$scope', function ($scope) {
        $scope.searchTab = 'services';

        $scope.setTab = function(newTab){
            $scope.searchTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.searchTab === tabNum;
        };
    }]);



//======================================================================================================================
//======================================================================================================================
//======================================================================================================================

    app.directive("fabAccordion", function () {
        return {
            restrict: "A",
            link: function () {
                $('.search-content-tabs-box-accordion-title').on("click", function (e) {
                    var thisItemContent = $(this).next();

                    $(this).toggleClass('open');

                    $('.search-content-tabs-box-accordion-item.open').not(thisItemContent).removeClass('open').removeAttr('style');
                    thisItemContent.toggleClass('open');

                    if (thisItemContent.hasClass('open')) {
                        thisItemContent.css({height: thisItemContent.height()});
                    } else  {
                        thisItemContent.removeAttr('style');
                    }
                });
            }
        };
    });

})();