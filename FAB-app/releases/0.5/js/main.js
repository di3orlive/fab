'use strict';

(function() {
    var app = angular.module('fabApp', []);

    app.controller('tabCtrl', ['$scope', '$http', function ($scope, $http) {
        $scope.searchTab = 'services';

        $scope.setTab = function(newTab){
            $scope.searchTab = newTab;
        };
        $scope.isSet = function(tabNum){
            return $scope.searchTab === tabNum;
        };



        $http.get('services.json').success(function(response) {
            //debugger;
            $scope.servicesData = response.services;
        });
    }]);

//======================================================================================================================
//======================================================================================================================
//======================================================================================================================

    app.directive("fabAccordion", function ($timeout) {
        return {
            restrict: "A",
            link: function () {
                var onDomChange = function(){
                    $('.acc-title').on("click", function (e) {
                        var thisItemContent = $(this).next();


                        $('.acc-title.open').not($(this)).removeClass('open');
                        $('.acc-content.open').not(thisItemContent).removeClass('open').removeAttr('style');
                        thisItemContent.toggleClass('open');
                        $(this).toggleClass('open');


                        if (thisItemContent.hasClass('open')) {
                            thisItemContent.css({height: thisItemContent.height()});
                        } else  {
                            thisItemContent.removeAttr('style');
                        }
                    });
                };
                $timeout(onDomChange, 0);
            }
        };
    });

})();